# Weather Gadget

This is a weather gadget which allow users to get current and forecast weather base on location. React framework has been adapted in this project

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

```
git clone git@gitlab.com:tsecheukfung01/my-weather-gadget.git
```

## Installing

After you have cloned the files, there should be a frontend folder. Use the terminal and run the following commands

```
cd frontend
```

```
yarn install
```

<br />

Besides, please make an '.env' file under the directory of 'frontend' folder. You need to configure your open weather API key so that the fetching process and be run successfully. Please refer to the '.env sample' for configuration.

## Start the application

### For frontend

```
cd frontend
```

```
yarn start
```

Therefore, you can start the application.
