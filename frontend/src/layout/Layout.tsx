import React from "react";
import "./layout.css";
import LocationSection from "../component/LocationSection/LocationSection";
import CurrentWeatherSection from "../component/CurrentWeatherSection/CurrentWeatherSection";
import { IRootState, ThunkDispatch } from "../redux/store";
import {
    getCurrentWeather,
    getForecastWeather
} from "../redux/WeatherSection/thunk";
import { connect } from "react-redux";
import ForecastWeatherSection from "../component/ForecastWeatherSection/ForecastWeatherSection";
import { City, CountryData } from "../generalInterface";
import { updateCity, updateCountryCode } from "../redux/WeatherSection/action";

export interface ILayoutState {
    countryList: any;
    cityList: Array<City>;
}

export interface ILayoutProps {
    city: string;
    countryCode: string;
    getCurrentWeather: (cityName: string) => void;
    getForecastWeather: (cityName: string, countryCode: string) => void;
    updateCity: (city: string) => void;
    updateCountryCode: (countryCode: string) => void;
}

class Layout extends React.Component<ILayoutProps, ILayoutState> {
    constructor(props: ILayoutProps) {
        super(props);
        this.state = {
            cityList: [],
            countryList: []
        };
    }
    getCountryList = async () => {
        const response = await fetch(
            `https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/slim-2/slim-2.json`
        );
        const dataFromWeb: Array<CountryData> = await response.json();
        return dataFromWeb;
    };

    filterCityList = (countryCode: string) => {
        const dataFromFile = require(`./cityList_Original.json`);
        const filterResult = dataFromFile.filter(
            (elem: City) => elem.country === countryCode
        );
        return filterResult;
    };

    componentDidMount = async () => {
        this.setState({
            countryList: await this.getCountryList()
        });
        this.props.getCurrentWeather(this.props.city);
        this.props.getForecastWeather(this.props.city, this.props.countryCode);
    };

    componentDidUpdate = (
        prevProps: ILayoutProps,
        _prevState: ILayoutState
    ) => {
        if (prevProps.countryCode !== this.props.countryCode) {
            this.filterCityList(this.props.countryCode);
        }

        if (prevProps.city !== this.props.city) {
            this.props.getCurrentWeather(this.props.city);
            this.props.getForecastWeather(
                this.props.city,
                this.props.countryCode
            );
        }
    };

    render = () => {
        return (
            <div className="container">
                <LocationSection
                    city={this.props.city}
                    country={this.props.countryCode}
                    filterCityList={this.filterCityList}
                    cityList={this.filterCityList(
                        this.props.countryCode || "HK"
                    )}
                    countryList={this.state.countryList}
                />
                <hr />
                <CurrentWeatherSection />
                <hr />
                <ForecastWeatherSection />
            </div>
        );
    };
}

const mapStateToProps = (state: IRootState) => {
    return {
        city: state.weatherSection.city,
        countryCode: state.weatherSection.countryCode
    };
};

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        // changeLocationState: (currentState: string) => {dispatch(changeLocationState(currentState))}
        updateCity: (city: string) => dispatch(updateCity(city)),
        updateCountryCode: (countryCode: string) =>
            dispatch(updateCountryCode(countryCode)),
        getCurrentWeather: (cityName: string) =>
            dispatch(getCurrentWeather(cityName)),
        getForecastWeather: (cityName: string, countryCode: string) =>
            dispatch(getForecastWeather(cityName, countryCode))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Layout);
