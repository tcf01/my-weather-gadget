import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk, { ThunkDispatch } from "redux-thunk"
import { composeEnhancers } from "./debugger"
import { logger } from "redux-logger"
import "./debugger"
import { IWeatherSectionState } from "./WeatherSection/state"
import { weatherReducer } from "./WeatherSection/reducer"
import { IWeatherSectionActions } from "./WeatherSection/action"

export interface IRootState {
    weatherSection: IWeatherSectionState
}

export type IRootAction = IWeatherSectionActions
export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>

const rootReducer = combineReducers<IRootState>({
    weatherSection: weatherReducer
})

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(applyMiddleware(logger), applyMiddleware(thunk))
)
