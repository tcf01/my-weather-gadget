import {
    updateCurrentWeather,
    IWeatherSectionActions,
    updateForecastWeather,
    changeIsLoading
} from "./action";
import { Dispatch } from "redux";

const { REACT_APP_OPEN_WEATHER_API_KEY } = process.env;

export function getCurrentWeather(cityName: string) {
    return async (dispatch: Dispatch<IWeatherSectionActions>) => {
        const response = await fetch(
            `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${REACT_APP_OPEN_WEATHER_API_KEY}`
        );
        const dataFromWeb = await response.json();
        dispatch(updateCurrentWeather(dataFromWeb));
    };
}

export function getForecastWeather(cityName: string, countryCode: string) {
    return async (dispatch: Dispatch<IWeatherSectionActions>) => {
        await dispatch(changeIsLoading());
        const response = await fetch(
            `http://api.openweathermap.org/data/2.5/forecast?q=${cityName},${countryCode}&units=metric&appid=${REACT_APP_OPEN_WEATHER_API_KEY}`
        );
        const dataFromWeb = await response.json();
        dispatch(updateForecastWeather(dataFromWeb));
    };
}
