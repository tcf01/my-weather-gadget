export function changeIsLoading() {
    return {
        type: "@@weather/TOGGLE_LOADING_STATUS" as "@@weather/TOGGLE_LOADING_STATUS"
    };
}

export function updateCurrentWeather(dataFromWeb: any) {
    return {
        type: "@@weather/UPDATE_CURRENT_WEATHER" as "@@weather/UPDATE_CURRENT_WEATHER",
        dataFromWeb
    };
}

export function updateForecastWeather(dataFromWeb: any) {
    return {
        type: "@@weather/UPDATE_FORECAST_WEATHER" as "@@weather/UPDATE_FORECAST_WEATHER",
        dataFromWeb
    };
}

export function updateCity(city: string) {
    return {
        type: "@@weather/UPDATE_CITY" as "@@weather/UPDATE_CITY",
        city
    };
}

export function updateCountryCode(countryCode: string) {
    return {
        type: "@@weather/UPDATE_COUNTRY_CODE" as "@@weather/UPDATE_COUNTRY_CODE",
        countryCode
    };
}

// export function changeLocationState(locationState: string){
//     return {
//         type: "@@weather/ASSIGN_NEW_COUNTRY_CODE" as "@@weather/UPDATE_COUNTRY_CODE",
//         locationState
//     }
// }

type WeatherActionsCreator =
    | typeof updateCurrentWeather
    | typeof updateForecastWeather
    | typeof updateCity
    | typeof updateCountryCode
    | typeof changeIsLoading;
export type IWeatherSectionActions = ReturnType<WeatherActionsCreator>;
