import { IWeatherSectionActions } from "./action";
import { IWeatherSectionState } from "./state";

const initialState = {
    city: "Kowloon City",
    countryCode: "HK",
    current: {
        weather: [] as any,
        main: {} as any,
        wind: [] as any
    },
    forecast: { list: [] as any },
    isLoading: true
};

export function weatherReducer(
    state: IWeatherSectionState = initialState,
    action: IWeatherSectionActions
): IWeatherSectionState {
    switch (action.type) {
        case "@@weather/TOGGLE_LOADING_STATUS":
            return {
                ...state,
                isLoading: !state.isLoading
            };
        case "@@weather/UPDATE_CITY":
            return {
                ...state,
                city: action.city
            };
        case "@@weather/UPDATE_COUNTRY_CODE":
            return {
                ...state,
                countryCode: action.countryCode
            };
        case "@@weather/UPDATE_CURRENT_WEATHER":
            return {
                ...state,
                current: action.dataFromWeb,
                isLoading: false
            };
        case "@@weather/UPDATE_FORECAST_WEATHER":
            return {
                ...state,
                forecast: action.dataFromWeb,
                isLoading: false
            };
        default:
            return state;
    }
}
