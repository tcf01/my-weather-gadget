import { City } from "../../generalInterface";

export interface CurrentWeather {
    weather: [
        {
            id: number;
            main: string;
            description: string;
            icon: string;
        }
    ];
    main: {
        temp: number;
        pressure: number;
        humidity: number;
        temp_min: number;
        temp_max: number;
    };

    wind: {
        speed: number;
        deg: number;
    };
}

export interface ForecastWeather {
    list: [
        {
            main: {
                temp: number;
                humidity: number;
                temp_max: number;
            };
            weather: [
                {
                    id: number;
                    main: string;
                    description: string;
                    icon: string;
                }
            ];
            dt_txt: string;
        }
    ];
}

export interface IWeatherSectionState {
    city: string;
    countryCode: string;
    current: CurrentWeather;
    forecast: ForecastWeather;
    isLoading: boolean;
}
