import * as React from "react";
import { connect } from "react-redux";
import { IRootState } from "../../redux/store";
import "./ForecastWeatherSection.css";
import { ForecastWeather } from "../../redux/WeatherSection/state";
import moment from "moment";
import { Spinner } from "../Spinner";

export interface IForecastWeatherSectionProps {
    forecast: ForecastWeather;
    isLoading: boolean;
}

interface IOrganizeData {
    main: {
        temp: number;
        humidity: number;
        temp_max: number;
    };
    weather: [
        {
            id: number;
            main: string;
            description: string;
            icon: string;
        }
    ];
    dt_txt: string;
}

interface Weather {
    icon: string;
    maxTemp: string;
    humidity: number;
    date: string;
    time: string;
}

class ForecastWeatherSection extends React.Component<
    IForecastWeatherSectionProps
> {
    classifiedAccordingToKey = (
        weathers: Array<Weather>,
        key: keyof Weather
    ) => {
        const newObj: { [key: string]: Array<Weather> } = {};
        weathers.forEach(weather => {
            const array: Array<Weather> = newObj[weather[key]] || [];
            array.push(weather);
            newObj[weather[key]] = array;
        });

        return newObj;
    };

    render = () => {
        const { forecast } = this.props;
        const dataNeeded = forecast.list.map(elem => {
            return {
                icon: elem.weather[0].icon,
                maxTemp: Number(elem.main.temp_max).toFixed(1),
                humidity: Number(elem.main.humidity),
                date: moment(elem.dt_txt.split(" ")[0]).format("MMMM D YYYY"),
                time: elem.dt_txt.split(" ")[1]
            };
        });

        const organizeData = this.classifiedAccordingToKey(dataNeeded, "date");
        const finalResult = Object.keys(organizeData).map(function(key) {
            return [key, organizeData[key]];
        });
        return (
            <div className="forecastWeatherSection">
                <h1>Forecast</h1>
                {this.props.isLoading === true ? (
                    <Spinner />
                ) : (
                    finalResult.map((elem: any) => (
                        <div key={Math.random()}>
                            <div>{elem[0]}</div>
                            <div>
                                {(elem[1] as Array<Weather>).map(elem => (
                                    <div
                                        className="weatherDetail"
                                        key={Math.random()}
                                    >
                                        <div className="time">{elem.time}</div>
                                        <div className="icon">
                                            <img
                                                src={`http://openweathermap.org/img/wn/${elem.icon}@2x.png`}
                                                alt=""
                                            />
                                        </div>
                                        <div className="humidity">
                                            {elem.humidity} %
                                        </div>
                                        <div className="maxTemp">
                                            {elem.maxTemp} °C
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    ))
                )}
                }}
            </div>
        );
    };
}

const mapStateToProps = (state: IRootState) => {
    return {
        forecast: state.weatherSection.forecast,
        isLoading: state.weatherSection.isLoading
    };
};

// export default CurrentWeatherSection
export default connect(mapStateToProps)(ForecastWeatherSection);
