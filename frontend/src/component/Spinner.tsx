import * as React from "react";
import "./Spinner.css";

export const Spinner: React.FC<{}> = () => {
    return (
        <div className="spinner_section">
            <div className="lds-spinner">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <span className="loadingText">Loading</span>
        </div>
    );
};
