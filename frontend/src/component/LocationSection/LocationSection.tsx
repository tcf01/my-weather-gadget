import * as React from "react";
import PopupMenu from "../PopupMenu/PopupMenu";
import "./LocationSection.css";
import { City, CountryData } from "../../generalInterface";

interface ILocationSectionProps {
    filterCityList: (countryCode: string) => void;
    city: string;
    country: string;
    countryList: Array<CountryData>;
    cityList: Array<City>;
}

class LocationSection extends React.Component<ILocationSectionProps> {
    countryObject = this.props.countryList.find(
        elem => elem["alpha-2"] === this.props.country
    );

    render = () => {
        return (
            <>
                <div className="buttonsGroup">
                    <PopupMenu
                        buttonName={"Change Location"}
                        className="changeLocation"
                        filterCityList={this.props.filterCityList}
                        countryList={this.props.countryList}
                    />
                </div>
                <div className="recentLocation">
                    <span className="city">{this.props.city}</span>,
                    <span className="country">
                        {this.props.countryList
                            .filter(
                                elem => elem["alpha-2"] === this.props.country
                            )
                            .map(elem => elem.name)}
                    </span>
                </div>
            </>
        );
    };
}

export default LocationSection;
