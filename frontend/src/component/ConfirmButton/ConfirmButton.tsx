import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import "./confirmButton.css";

const PopupModal = (props: any) => {
    const { modal, setModal } = props;
    const toggle = () => setModal(!modal);

    return (
        <div>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>
                    <h1 style={{ color: "red" }}>Alert ! !</h1>
                </ModalHeader>
                <ModalBody>
                    <h3>
                        Please also select the 'city' in order to get the
                        accurate data
                    </h3>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        Change now
                    </Button>{" "}
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default PopupModal;
