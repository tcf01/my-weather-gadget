import React, { useState } from "react";
import { Button, Modal, ModalHeader } from "reactstrap";
import { connect } from "react-redux";
import { IRootState, ThunkDispatch } from "../../redux/store";
import { CountryData, City } from "../../generalInterface";
import "./PopupMenu.css";
import {
    updateCity,
    updateCountryCode
} from "../../redux/WeatherSection/action";
import PopupModal from "../ConfirmButton/ConfirmButton";

const PopupMenu: React.FC<any> = props => {
    const {
        city,
        className,
        buttonName,
        countryList,
        updateCity,
        updateCountryCode,
        countryCode,
        filterCityList
    } = props;
    const [modal, setModal] = useState(false);
    const [tempRecentCity, setTempCity] = useState("");
    const [tempRecentCountry, setRecentCountry] = useState("");
    const [modal2, setModal2] = useState(false);

    const toggle = () => setModal(!modal);

    const recentCityList = filterCityList(countryCode);
    const handleClick = (tempRecentCity: string) => {
        if (tempRecentCity !== "" && city !== tempRecentCity) {
            updateCity(tempRecentCity === "" ? "Kowloon City" : tempRecentCity);
        } else if (
            countryCode !== tempRecentCountry &&
            city === tempRecentCity
        ) {
        } else {
            setModal2(!modal2);
        }
    };

    return (
        <div>
            <Button color="danger" onClick={toggle}>
                {buttonName}
            </Button>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}></ModalHeader>
                <div className="countrySection">
                    <div className="text">Country</div>
                    <div className="optionCountry">
                        <select
                            name="country"
                            id="location"
                            value={countryCode}
                            onChange={event => {
                                updateCountryCode(event.currentTarget.value);
                                setRecentCountry(event.currentTarget.value);
                            }}
                        >
                            {countryList &&
                                countryList.map((elem: CountryData) => (
                                    <option
                                        label={elem.name}
                                        value={elem["alpha-2"]}
                                        key={Math.random()}
                                    ></option>
                                ))}
                        </select>
                    </div>
                </div>
                <div className="citySection">
                    <div className="text">City</div>
                    <div className="optionCity">
                        <select
                            name="city"
                            id="location"
                            value={tempRecentCity}
                            onChange={(
                                event: React.FormEvent<HTMLSelectElement>
                            ) => {
                                setTempCity(event.currentTarget.value);
                            }}
                        >
                            <option>Please select the city</option>
                            {recentCityList &&
                                recentCityList.map((elem: City) => (
                                    <option
                                        key={Math.random()}
                                        value={elem.name}
                                        label={elem.name}
                                    ></option>
                                ))}
                        </select>
                    </div>
                    <Button
                        disabled={tempRecentCity === "Please select the city"}
                        className="updateWeatherButton"
                        onClick={handleClick.bind(null, tempRecentCity)}
                    >
                        Update
                    </Button>
                    <PopupModal
                        modal={modal2}
                        turnOn={true}
                        setModal={setModal2}
                    />
                </div>
            </Modal>
        </div>
    );
};

const mapStateToProps = (state: IRootState) => {
    return {
        city: state.weatherSection.city,
        countryCode: state.weatherSection.countryCode
    };
};

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        updateCity: (city: string) => dispatch(updateCity(city)),
        updateCountryCode: (countryCode: string) =>
            dispatch(updateCountryCode(countryCode))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PopupMenu);
