import * as React from "react";
import { connect } from "react-redux";
import "./CurrentWeatherSection.css";
import { IRootState } from "../../redux/store";
import { CurrentWeather } from "../../redux/WeatherSection/state";
import { Spinner } from "../Spinner";
import { Dispatch } from "redux";
import { changeIsLoading } from "../../redux/WeatherSection/action";

export interface ICurrentWeatherSectionProps {
    current: CurrentWeather;
    isLoading: boolean;
}

class CurrentWeatherSection extends React.Component<
    ICurrentWeatherSectionProps
> {
    render = () => {
        const { current } = this.props;

        const dataNeeded = {
            temperature: Number(current.main.temp).toFixed(1),
            weatherDescription: current.weather.map(elem => elem.main),
            weatherSubDescription: current.weather.map(
                elem => elem.description
            ),
            humidity: current.main.humidity,
            windDegree: current.wind.deg,
            windSpeed: current.wind.speed,
            icon: current.weather.map(elem => elem.icon)
        };
        return (
            <div className="currentWeatherSection">
                <h1>Current Weather</h1>
                {this.props.isLoading === true ? (
                    <Spinner />
                ) : (
                    <div className="currentWeatherInfo row">
                        <img
                            src={`http://openweathermap.org/img/wn/${
                                dataNeeded.icon.length > 0
                                    ? dataNeeded.icon
                                    : "10d"
                            }@2x.png`}
                            alt=""
                            className="col-4"
                        />
                        <div className="weatherDetail col">
                            <div className="tempAndWeatherDescription">
                                {dataNeeded.temperature}°C,{" "}
                                {dataNeeded.weatherDescription}
                            </div>
                            <div className="weatherSubDescription">
                                {dataNeeded.weatherSubDescription}
                            </div>
                            <div className="humidity">
                                Humidity: {dataNeeded.humidity}%
                            </div>
                            <div className="wind">
                                Wind: {dataNeeded.windDegree}{" "}
                                {dataNeeded.windSpeed}
                                m/s
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    };
}

const mapStateToProps = (state: IRootState) => {
    return {
        current: state.weatherSection.current,
        isLoading: state.weatherSection.isLoading
    };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        changeIsLoading: () => dispatch(changeIsLoading())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrentWeatherSection);
