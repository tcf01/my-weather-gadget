import React from "react"
import { Provider } from "react-redux"
import store from "./redux/store"
import "bootstrap/dist/css/bootstrap.css"
import Layout from "./layout/Layout"

class App extends React.Component {
    render = () => {
        return (
            <div className="App">
                <Provider store={store}>
                    <Layout />
                </Provider>
            </div>
        )
    }
}

export default App
