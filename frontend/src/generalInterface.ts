export interface City {
    coord: { lon: number; lat: number };
    country: string;
    id: number;
    name: string;
}
export interface CountryData {
    "alpha-2": string;
    "country-code": string;
    name: string;
}
